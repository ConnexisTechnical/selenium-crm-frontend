import { Component, OnInit } from '@angular/core';
declare  let $:any;
@Component({
  selector: 'app-calls',
  templateUrl: './calls.component.html',
  styleUrls: ['./calls.component.css']
})
export class CallsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('#tableClients').bootstrapTable();
  }

}
