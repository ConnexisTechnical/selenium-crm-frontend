import { Component, OnInit } from '@angular/core';

declare let $:any;
@Component({
  selector: 'app-fav-list',
  templateUrl: './fav-list.component.html',
  styleUrls: ['./fav-list.component.css']
})
export class FavListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('#tableClients').bootstrapTable();
    $('#tableContact').bootstrapTable();
  }

}
