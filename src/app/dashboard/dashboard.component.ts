import { Component, OnInit } from '@angular/core';

declare let $:any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('#tableRecentAddition').bootstrapTable();
    $('#tableRecentCalls').bootstrapTable();
    $('#tableActiveShared').bootstrapTable();

    $(".fa-minus-circle").click(function () {
      $(this).toggleClass("fa-plus-circle");
  })
  $(".rotate").click(function () {
    $(this).toggleClass("down");
  })
  }

}
