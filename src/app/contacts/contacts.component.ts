import { Component, OnInit } from '@angular/core';
declare  let $:any;
@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('#tableContact').bootstrapTable();
  }

}
