import { Component, OnInit } from '@angular/core';

declare let $;
@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css']
})
export class ListsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('#tableClients').bootstrapTable();
  }

}
