function openNav() {
    document.getElementById("sideNavigation").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    document.getElementById("main").style.width = "85%";
    document.getElementById("nav-menu").style.marginLeft = "200px";
    document.getElementById("left-side-icon-menu").style.marginLeft = "250px";
    document.getElementById("footer-box").style.marginLeft = "250px";
    // document.getElementById("main").style.backgroundColor = "rgba(0,0,0,0.3)";
}
 
function closeNav() {
    document.getElementById("sideNavigation").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
    document.getElementById("main").style.width = "100%";
    document.getElementById("nav-menu").style.marginLeft = "0";
    document.getElementById("left-side-icon-menu").style.marginLeft = "0";
    document.getElementById("footer-box").style.marginLeft = "0";
    // document.getElementById("main").style.backgroundColor = "#fff";
}
// $(".rotate").click(function () {
//     $(this).toggleClass("down");
// })

