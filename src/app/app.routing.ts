import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { ClientsComponent } from './clients/clients.component';
import { ContactsComponent } from './contacts/contacts.component';
import { CallsComponent } from './calls/calls.component';
import { FavListComponent } from './fav-list/fav-list.component';
import { ListsComponent } from './lists/lists.component';
import { ContactDetailsComponent } from './contact-details/contact-details.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'dashboard',pathMatch: 'full'},
  { path: 'dashboard', component: DashboardComponent },
  { path: 'login', component: LoginComponent },
  { path: 'clients', component: ClientsComponent },
  { path: 'contacts', component: ContactsComponent },
  { path: 'calls', component: CallsComponent },
  { path: 'favList', component: FavListComponent },
  { path: 'lists', component: ListsComponent },
  { path: 'contact_details', component: ContactDetailsComponent }

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
